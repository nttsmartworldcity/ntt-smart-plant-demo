(function(window) {
  console.log("reached env.js");
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["apiUrlProd"] = "http://bb-demo-k8s.lvsc.local:30023/";
  window["env"]["apiUrlLocal"] = "http://localhost:3002/";
  window["env"]["debug"] = true;
})(this);