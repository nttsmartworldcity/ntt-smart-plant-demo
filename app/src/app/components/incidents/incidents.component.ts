import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { environment } from '@env/environment';
declare var jQuery: any;
@Component({
  selector: 'app-incidents',
  templateUrl: './incidents.component.html',
  styleUrls: ['./incidents.component.css']
})
export class IncidentsComponent implements OnInit {
  linedata = [];
  alerts =[];// environment.Alerts;
  report:any;// =environment.Report;
  constructor() {
      this.loadviolation();
      this.loadalerts();
      this.loadreport();
      setTimeout(() => {
          this.bindevents();
      }, 3000);
  }
  ngOnInit(): void {
      
  }
  bindevents()
  {
      (function ($) {
          $(document).ready(function () {
            $('.card-link1').click(function () {
                // $(this).find('i').toggleClass('fa fa-angle-down fa fa-angle-up');
                var icon = $(this).find('.material-icons');
                 icon.toggleClass('up');
                if ( icon.hasClass('up') ) {
                    icon.text('expand_less');
                  } else {
                    icon.text('expand_more');
                  }
            });
              var coll = document.getElementsByClassName("collapsible");
              var i;

              for (i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function () {
                      this.classList.toggle("active");
                      var content = this.nextElementSibling;
                      if (content.style.maxHeight) {
                          content.style.maxHeight = null;
                      } else {
                          content.style.maxHeight = content.scrollHeight + "px";
                      }
                  });
              }
              var coll = document.getElementsByClassName("collapsible1");
              var i;

              for (i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function () {
                      this.classList.toggle("active");
                      var content = this.nextElementSibling;
                      if (content.style.display === "block") {
                          content.style.display = "none";
                      } else {
                          content.style.display = "block";
                      }
                  });
              }
          });
      })(jQuery);
  }
  loadviolation() {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    try {
        axios.get(environment.apiUrl + `api/violations`, { cancelToken: source.token })
          .then(data => {
            // Update the status of each health screen based on the data received
            let dataArray = data.data;
            this.linedata = dataArray;
            console.log("got the violations data: " + dataArray);
          });
      } catch (error) {
        if (axios.isCancel(error)) {        
        } else {
          throw error;
        }
    }  
  }
  loadalerts() {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    try {
        axios.get(environment.apiUrl + `api/alerts`, { cancelToken: source.token })
          .then(data => {
            // Update the status of each health screen based on the data received
            let dataArray = data.data;
            this.alerts = dataArray;
            console.log("got the alerts data: " + dataArray);
          });
      } catch (error) {
        if (axios.isCancel(error)) {        
        } else {
          throw error;
        }
    }
  }
  loadreport() {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    try {
        axios.get(environment.apiUrl + `api/report`, { cancelToken: source.token })
          .then(data => {
            // Update the status of each health screen based on the data received
            let dataArray = data.data;
            this.report = dataArray;
            console.log("got the report data: " + dataArray);
          });
      } catch (error) {
        if (axios.isCancel(error)) {        
        } else {
          throw error;
        }
    }
    }
}
