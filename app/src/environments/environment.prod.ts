export const environment = {
  production: true,
  apiUrl: window["env"]["apiUrlProd"] || "default",
  debug: window["env"]["debug"] || false
};
