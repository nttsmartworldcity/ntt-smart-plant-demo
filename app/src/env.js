(function (window) {
  console.log("loaded the env.js file");
  window.__env = window.__env || {};

  // API url
  //PLANT_SAFETY_REPLACE
  window.__env.apiURL = 'http://' + 'bb-demo-k8s.lvsc.local:30023' + '/';
  // Base url
  window.__env.baseURL = '/';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));