#FROM nginx:1.19.2
#COPY nginx.conf /etc/nginx/nginx.conf
#COPY app/dist/BuildingBlocks /usr/share/nginx/html

FROM node:14.8
COPY app/dist/BuildingBlocks /usr/src/app
COPY app_server/ /usr/src/app
WORKDIR /usr/src/app
RUN yarn install
RUN yarn global add serve
RUN npm install -g nodemon
RUN apt-get update
RUN apt-get -y install supervisor && mkdir -p /var/log/supervisor && mkdir -p /etc/supervisor/conf.d
RUN touch /tmp/supervisor.sock
RUN mkdir -p /var/log/supervisord
RUN touch /var/log/supervisord/supervisord.log
RUN chmod 777 /tmp/supervisor.sock
ADD supervisor.conf /etc/supervisor.conf
EXPOSE 5000 3002
CMD ["supervisord", "-c", "/etc/supervisor.conf"]
