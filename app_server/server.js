// Require our dependencies
const cors = require('cors');
const express = require('express');
const app = express();

app.use(express.json());

/* Set up CORS header to allow any URL */
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
// app.use(cors());

/* GET Angular App */
const path = require("path");
app.use(express.static(path.join(__dirname, "..", "usr/src/app")));
app.use(express.static("public"));
console.log("Finished adding the React app's path");

// Assign a port based on either an environment variable (if existing) or 3002
const PORT = process.env.PORT || 3002;

// Data arrays
const posts = [
  {
    "id": 1,
    "title": "hello"
  }
]

const profile = {
  "name": "typicode"
}

const violations = [
  {
    "id": 0,
    "type": "line",
    "title": "Risk index",
    "subtitle": "Main Facility",
    "count": "51",
    "countof": "/100",
    "period": "2.4%",
    "perioddesc": "Less than last month",
    "svgiconstyle": "transform:scaleY(1)",
    "iconimg": "assets/icons/arrow_downward.svg",
    "roundclass": "roundyellow",
    "svgcolor": "green",
    "video": "assets/videos/1.png"
  },
  {
    "id": 1,
    "type": "line",
    "title": "Safety line violations",
    "subtitle": "Main Facility",
    "count": "58",
    "countof": "Total alerts per month",
    "secondcount": "12",
    "secondcountof": "Alerts",
    "period": "4%",
    "perioddesc": "Less than last month",
    "svgiconstyle": "transform:scaleY(1)",
    "iconimg": "assets/icons/arrow_downward.svg",
    "roundclass": "roundgreen",
    "svgcolor": "green",
    "video": "assets/videos/2.png"
  },
  {
    "id": 2,
    "type": "line",
    "title": "Crowd violations",
    "subtitle": "Main Facility",
    "count": "28",
    "countof": "Total alerts per month",
    "period": "3%",
    "perioddesc": "More than last month",
    "svgiconstyle": "transform:scaleY(-1)",
    "iconimg": "assets/icons/arrow_downward.svg",
    "roundclass": "roundred",
    "svgcolor": "red",
    "video": "assets/videos/3.png"
  },
  {
    "id": 3,
    "type": "employee",
    "title": "Employees scanned",
    "subtitle": "Main Facility",
    "count": "236",
    "countof": "Total alerts per month",
    "secondcount": "19",
    "secondcountof": "Alerts",
    "period": "7%",
    "perioddesc": "More than yesterday",
    "svgiconstyle": "transform:scaleY(-1)",
    "iconimg": "assets/icons/arrow_downward.svg",
    "roundclass": "roundred",
    "svgcolor": "red",
    "video": "assets/videos/4.png"
  }
]

const alerts = [
  {
    "id": 0,
    "title": "Safety line crossing",
    "time": "15:28:06",
    "place": "Workstation 14",
    "count": "1 person"
  },
  {
    "id": 1,
    "title": "Crowd violations",
    "time": "15:18:06",
    "place": "Workstation 09",
    "count": "3 people"
  },
  {
    "id": 2,
    "title": "Safety line crossing",
    "time": "14:28:06",
    "place": "Workstation 5",
    "count": "1 person"
  },
  {
    "id": 3,
    "title": "Safety line crossing",
    "time": "14:05:06",
    "place": "Workstation 14",
    "count": "3 people"
  },
  {
    "id": 4,
    "title": "Crowd violations",
    "time": "13:28:06",
    "place": "Entrance",
    "count": "2 people"
  },
  {
    "id": 5,
    "title": "Safety line crossing",
    "time": "13:15:06",
    "place": "Workstation 6",
    "count": "2 people"
  },
  {
    "id": 6,
    "title": "Safety line crossing",
    "time": "12:28:06",
    "place": "Workstation 9",
    "count": "2 people"
  }
]

const report = {
  "addr1": "06",
  "addr2": "05",
  "addr3": "06"
}

const enrollments = [
  {
    "id": 1,
    "name": "Dhiraj",
    "status": true,
    "dobDate": "22-03-1981"
  },
  {
    "id": 2,
    "name": "Naresh",
    "status": true,
    "dobDate": "12-06-1988"
  },
  {
    "id": 3,
    "name": "Kumar",
    "status": false,
    "dobDate": "02-05-1991"
  },
  {
    "id": 4,
    "name": "Karthik",
    "status": true,
    "dobDate": "09-03-1981"
  },
  {
    "id": 5,
    "name": "Rajesh",
    "status": true,
    "dobDate": "23-08-1985"
  },
  {
    "id": 6,
    "name": "Prakash",
    "status": true,
    "dobDate": "12-09-1983"
  }
]

/*
* Plant Safety Routes
*/ 
// GET Requests
// Posts
app.get('/api/posts', (req, res) => {
  res.send(posts);
})
app.get('/api/posts/:id', (req, res) => {
  const post = posts.find(p => p.id === parseInt(req.params.id));

  if (!post) return res.status(404).send('The post with the given ID was not found.');

  res.send(post);
})

// Profile
app.get('/api/profile', (req, res) => {
  res.send(profile);
})

// Violations
app.get('/api/violations', (req, res) => {
  res.send(violations);
})
app.get('/api/violations/:id', (req, res) => {
  const violation = violations.find(v => v.id === parseInt(req.params.id));

  if (!violation) return res.status(404).send('The violation with the given ID was not found.');

  res.send(violation);
})

// Alerts
app.get('/api/alerts', (req, res) => {
  res.send(alerts);
})
app.get('/api/alerts/:id', (req, res) => {
  const alert = alerts.find(a => a.id === parseInt(req.params.id));

  if (!alert) return res.status(404).send('The alert with the given ID was not found.');

  res.send(alert);
})

// Report
app.get('/api/report', (req, res) => {
  res.send(report);
})

// Enrollments
app.get('/api/enrollment', (req, res) => {
  res.send(enrollment);
})
app.get('/api/alerts/:id', (req, res) => {
  const alert = alerts.find(a => a.id === parseInt(req.params.id));

  if (!alert) return res.status(404).send('The alert with the given ID was not found.');

  res.send(alert);
})

// Make the server listen for requests
app.listen(PORT, () => console.log(`Running on port ${PORT}`));